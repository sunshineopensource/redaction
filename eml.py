#!/usr/bin/env python3

# Copyright (c) 2019 by the authors - https://gitlab.com/sunshineopensource/redaction
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# THIS CODE HAS NOT UNDERGONE PEER REVIEW FOR CORRECTNESS OR SECURITY.
# NOTHING HEREIN IN LEGAL, SECURITY, OR ANY OTHER FORM OF PROFESSIONAL ADVICE.

"""Redacts .EML format emails using a whitelist of allowed headers."""

# Run as:
# python3 ./eml.py DIRECTORY/INPUT-EML-NAME.eml
# Will generate DIRECTORY/INPUT-EML-NAME.eml-Redacted.eml

import os
import email
import email.policy
import re

from argparse import ArgumentParser

class Config:
  def __init__(self, whitelist, remove_other_headers_reason, redact_received_reason):
    self.whitelist = whitelist
    self.remove_other_headers_reason = remove_other_headers_reason
    self.redact_received_reason = redact_received_reason

_received_header_pattern = re.compile(r'^from (?P<redacted>.*); (?P<time>.*)$')

def redact_headers(newmsg, config, attachment = False, keep_payload = False):
  # Headers are compared in a case-insensitive manner
  lwhitelist = [name.casefold() for name in whitelist]
  removed_headers = []
  received_values = []
  for k, v in newmsg.items():
    if k.casefold() not in lwhitelist:
      # Remove all headers not explicitly whitelisted
      removed_headers.append(k)
      del newmsg[k]
    elif k.casefold() == 'Received'.casefold():
      # Special handling of the Received header
      received_values.append(v)
      del newmsg[k]

  if len(received_values) > 0:
    for v in received_values:
      m = _received_header_pattern.match(v)
      if m:
        rv = 'from ' + config.redact_received_reason + '; ' + m.group('time')
        newmsg.add_header('Received', rv)
      else:
        newmsg.add_header('Received', config.redact_received_reason)

  if len(removed_headers) > 0:
    for k in removed_headers:
      newmsg.add_header(k, config.remove_other_headers_reason)

whitelist = [
  'Accept-Language', 'Auto-Submitted', 'Bcc', 'Cc', 'Content-Description', 'Content-ID',
  'Content-Disposition', 'Content-Language', 'Content-Transfer-Encoding', 'Content-Type',
  'Date', 'Delivered-To', 'From', 'In-Reply-To', 'Message-Id', 'Mime-Version',
  'Orig-Date', 'References',  'Reply-To', 'Resent-Date', 'Resent-To', 'Resent-Cc', 'Resent-Bcc',
  'Resent-From', 'Resent-Sender', 'Return-Path', 'Sender', 'Subject', 'Thread-Index',
  'Thread-Topic', 'To', 'X-Attached', 'X-Envelope-From', 'X-Forwarded-For', 'X-Forwarded-To',
  'X-Ms-Has-Attach', 'X-Original-To', 'X-Originatororg',
  # This field is specially handled above.
  'Received',
]
# If you wish to "delete" the information, instead of masking with clear reference,
# just make these empty strings instead.
remove_other_headers_reason = '[Redacted - Gov Code 6254.19]'
redact_received_reason = '[Redacted - Gov Code 6254.19]'

config = Config(whitelist, remove_other_headers_reason, redact_received_reason)

def main():
  parser = ArgumentParser()
  parser.add_argument('infiles', metavar='input-eml-file', type=str, nargs='+')
  args = parser.parse_args()

  for infile in args.infiles:
    with open(infile, 'rb') as fp:
      msg = email.message_from_binary_file(fp, policy=email.policy.default)

    # First we redact the headers of the top-level message
    redact_headers(msg, config)

    # Look at all subparts of the message
    for part in msg.walk():
      # Redact too the subpart headers
      redact_headers(part, config)
      # Preserve the containers
      if part.get_content_maintype() == 'multipart':
        continue
      # Preserve body of plain text portion.
      if part.get_content_maintype() == 'text' and part.get_content_subtype() == 'plain':
        continue
      
      # Delete the payload
      part.set_payload('')

    with open(infile + '-Redacted.eml', 'wb') as fp:
      fp.write(msg.as_bytes(policy=email.policy.HTTP))

if __name__ == '__main__':
    main()
